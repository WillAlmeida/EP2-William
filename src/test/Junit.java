package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ep2.Alien;
import ep2.Application;
import ep2.Game;
import ep2.Map;


public class Junit {

private Map map;
private Application app;
@SuppressWarnings("unused")
private List<Alien> aliens;
	@Before 
	public void setUp() {
		map = new Map(app);
		
	}
	
	//Teste do contador de condições dos Aliens
	@Test
	public void testAliens(){
		int count;
		Alien.setCount(5);
		count = Alien.getCount();
		assertEquals(5, count);
	}
	//Teste do Score inicial
	@Test
	public void testScore() {
		int score;
		score = map.getScore();
		assertEquals(0, score );		
	}
	//Teste do delay padrão do jogo
	@Test
	public void testDelay() {
		int delay;
		delay = Game.getDelay();
		assertEquals(10, delay);
	}
	//Teste da resolução do jogo
	@Test
	public void testResolution(){
		int height, width, resolution;
		height = Game.getHeight();
		width = Game.getWidth();
		resolution = height * width;
		assertEquals(250000, resolution);
	}
	//Testa retorno quando o player matar aliens
	@Test
	public void testDeadAliens(){
		map.setDeadAliens(3);
		int dA;
		dA = map.getDeadAliens();
		assertEquals(3, dA);
	}
	//Testa condição inicial do player no jogo
	@Test
	public void testIsAlive(){
		boolean iA;
		iA = map.isAlive();
		assertEquals(true, iA);
	}
	//Testa level inicial de jogo	
	@Test
	public void testPlayerLevel(){
		int level;
		level = map.getLevel();
		assertEquals(0, level);
	}
	
	
	
}
