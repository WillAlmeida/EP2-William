package ep2;
import javax.swing.JOptionPane;



/**
 *
 * @author William Almeida
 */
public class MainMenu extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;
	Application app;
    
    public MainMenu(Application app) {
        initComponents();
        this.app = app;
    }
    
    
    
    
    private void initComponents() {

        btnPlay = new javax.swing.JButton();
        btnCredits = new javax.swing.JButton();
        btnRanking = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        background = new javax.swing.JLabel();


        btnPlay.setText("PLAY");
        btnPlay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnPlayMousePressed(evt);
            }
        });
        btnPlay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayActionPerformed(evt);
            }
        });

        btnCredits.setText("CREDITS");
        btnCredits.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnCreditsMousePressed(evt);
            }
        });

        btnRanking.setText("RANKING");
        btnRanking.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnRankingMousePressed(evt);
            }
        });

        btnExit.setText("EXIT");
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnExitMousePressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
            this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(175, 175, 175)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRanking, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnPlay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCredits, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)))
                .addGap(175, 175, 175))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(123, 123, 123)
                .addComponent(btnPlay, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCredits, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRanking, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(123, 123, 123))
        );
        
        background.setIcon(new javax.swing.ImageIcon("images/space.jpg"));
        this.add(background);
        background.setBounds(0, 0, 500, 500);

        
    }                     

    private void btnPlayActionPerformed(java.awt.event.ActionEvent evt) {                                        
        // TODO add your handling code here:
    }                                       

    private void btnExitMousePressed(java.awt.event.MouseEvent evt) {                                     
        System.exit(0);
    }                                    

    private void btnRankingMousePressed(java.awt.event.MouseEvent evt) {                                        
    	
    	
        Ranking.loadRank();
        
    	
      }
    
    	
    	
                                          

    private void btnCreditsMousePressed(java.awt.event.MouseEvent evt) {                                        
        JOptionPane.showMessageDialog(null,"Developed by William Almeida \n ");
    }
    
    

    private void btnPlayMousePressed(java.awt.event.MouseEvent evt) {                                     
    	app.map = new Map(app);
        
        app.add(app.map);
        app.setSize(500,500);
        this.setVisible(false);
        app.map.requestFocusInWindow();
    }                                    

    
    
                   
    private javax.swing.JButton btnCredits;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnPlay;
    private javax.swing.JButton btnRanking;
    private javax.swing.JLabel background;
                     
}