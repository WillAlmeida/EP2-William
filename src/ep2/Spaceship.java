package ep2;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;


public class Spaceship extends Sprite {
    
    private static final int MAX_SPEED_X = 2;
    private static final int MAX_SPEED_Y = 1;
   
    private int speed_x;
    private int speed_y;
    
    private List<Missile> missiles;

    public Spaceship(int x, int y) {
        super(x, y);
        
        missiles = new ArrayList<>();
        
        initSpaceShip();
        
    }

    private void initSpaceShip() {
        
        noThrust();
        
    }
    
    public void explosion(){
    		loadImage("images/explosion.png");
    	
    }
    
    
    public void noThrust(){
        loadImage("images/spaceship.png"); 
    }
    
    private void thrust(){
        loadImage("images/spaceship_thrust.png"); 
    }    

    public void move() {
        
      
        if((speed_x < 0 && x <= 0) || (speed_x > 0 && x + width >= Game.getWidth())){
            speed_x = 0;
        }
        
      
        x += speed_x;
        
        
        if((speed_y < 0 && y <= 0) || (speed_y > 0 && y + height >= Game.getHeight())){
            speed_y = 0;
        }

        
        y += speed_y;
        
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        
        if (key == KeyEvent.VK_LEFT) { 
            speed_x = -1 * MAX_SPEED_X;
        }

      
        if (key == KeyEvent.VK_RIGHT) {
            speed_x = MAX_SPEED_X;
        }
        
       
        if (key == KeyEvent.VK_UP) {
            speed_y = -1 * MAX_SPEED_Y;
            thrust();
        }
        
      
        if (key == KeyEvent.VK_DOWN) {
            speed_y = MAX_SPEED_Y;
        }
        
        if (key == KeyEvent.VK_SPACE) {
			shoot();
        }
    }
    
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
            speed_x = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
            speed_y = 0;
                   
            noThrust();
        }
    }
    
    public void shoot(){
    	this.missiles.add(new Missile(this.x + this.width / 3, this.y + this.height));
    }
    
    public List<Missile> getMissiles() {
		return missiles;
	}
    
    public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}

	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setVisible(Boolean enable) {
        visible = enable;
    }
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
    
    
}