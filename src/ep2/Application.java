package ep2;
import java.awt.EventQueue;
import javax.swing.JFrame;


public class Application extends JFrame {
    
    
   
	private static final long serialVersionUID = 4192877259048280955L;
	public MainMenu mainScreen;
    public Map map;
        
        public void gameOver(){
            
            this.map.setVisible(false);
            this.mainScreen.setVisible(true);
            
        }
    public Application() {

        
        
        mainScreen = new MainMenu(this);
        add(mainScreen);
        
        setSize(Game.getWidth(), Game.getHeight());

        setTitle("EP2 - William");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        
    }
    
    public static void main(String[] args) {
        
    	EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Application app = new Application();
                app.setVisible(true);
            }
        });
    }
}