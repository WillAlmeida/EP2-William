package ep2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

public class Ranking {

	
	public static void saveRank(Map map){
	        
	    File arq = new File("src/ranking.txt");
	        try{
	            
	        	if(!arq.exists()){
	                arq.createNewFile();
	            }
	            
	            FileWriter fwriter = new FileWriter(arq, true);
	            BufferedWriter bwriter = new BufferedWriter(fwriter);
	            
	            bwriter.write("Score: " + map.getScore());
	            bwriter.newLine();
	            bwriter.close();
	            
	            fwriter.close();
	            
	        }catch(IOException ex){
	            ex.printStackTrace();
	        }	        
	        	
	}
	
	public static void loadRank(){
		String line = "";
		
		File arq = new File("src/ranking.txt");
        try{
        	
            if(!arq.exists()){
                arq.createNewFile();
            }
            
            FileReader fwriter = new FileReader(arq);
            
            @SuppressWarnings("resource")
			BufferedReader bwriter = new BufferedReader(fwriter);
            
            while(bwriter.ready()){
                line += bwriter.readLine();
                line += '\n';
            }
        
        }catch(IOException ex){
            ex.printStackTrace();
        };
        
        JOptionPane.showMessageDialog(null, line, "Ranking - EP2", JOptionPane.INFORMATION_MESSAGE);
	}
	
}