package ep2;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener {


	private static final long serialVersionUID = -1999244621083501313L;
	private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private final Timer timer_map;
    private final Image background;
    private final Spaceship spaceship;
    private List<Alien> aliens;
    private boolean isAlive;
    private int deadAliens;
    private int level = 0;
    private int score;
   

	private final Application app;
    
    public Map(Application app) {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.jpg");
        
        this.background = image.getImage();

        
        spawnAliens();
        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        
        timer_map = new Timer(Game.getDelay() , this);
        timer_map.start();
        
        isAlive = true;
        
        this.app = app;
    }
    
    public void spawnAliens(){
    	
    	Random generator = new Random();
    	
    	aliens = new ArrayList<Alien>();
    	
    	for(int i =0; i < 50; i++){
    		aliens.add(new Alien(generator.nextInt(480 - 20 + 1) + 20, - generator.nextInt(500 - 0 + 1) + 0));
    	
		}
    }
    
    
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null); 
       
        draw(g);

        Toolkit.getDefaultToolkit().sync();
    }

    private void draw(Graphics g) {
         
    	if(isAlive){
    		
    		
	        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
	        
	        List<Missile> missiles = spaceship.getMissiles();
	        
	        for (int i = 0; i < missiles.size(); i++) {
				Missile mi1 = (Missile) missiles.get(i);
				g.drawImage(mi1.getImage(), mi1.getX(), mi1.getY(), this);
				
			}
	        
	        for (int i = 0; i < aliens.size(); i++) {
				Alien a = aliens.get(i);
				g.drawImage(a.getImage(), a.getX(), a.getY(), this);
				
			}
	        
	        
	        g.setColor(Color.white);
	        g.drawString("Remaining aliens: " + aliens.size(), 50, 15);
	        g.drawString("Score: " + score, 350, 15);
	        

	        if (aliens.size() == 0 && level  < 3){
	        	dranLevelCompleted(g);
	        	dranPressEnter(g);
			}
	        if(level == 3){
	        	dranMissionAccomplished(g);
	        	showScore(g);
	        	dranNewGame(g);
	        	
	        	                  
	        }
	        
	        
	        
	        
    	}
    	else
    	{
    		g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
    		drawGameOver(g);
    		showScore(g);
    		dranPressEnter(g);
    	}
    		
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       
    	
    	
    	List<Missile> missiles = spaceship.getMissiles();

		for (int i = 0; i < missiles.size(); i++) {
			Missile mi2 = (Missile) missiles.get(i);

			if (mi2.isVisible()) {
				mi2.move();
			} 
			else {
				missiles.remove(i);
			}
		}

		for (int i = 0; i < aliens.size(); i++) {
			Alien a2 = aliens.get(i);

			if (a2.isVisible()) {
				a2.move();
			} else {
				aliens.remove(i);
			}
		}
		
		
        updateSpaceship();
        checkCollision();
        repaint();
    }
    
    private void dranLevelCompleted(Graphics g) {

        String message = "LEVEL COMPLETED";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    private void showScore(Graphics g) {

        String message = ("SCORE: " + score);
        Font font = new Font("Comic Sans", Font.BOLD, 18);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, 300);
    }
    
    private void dranMissionAccomplished(Graphics g) {

        String message = "MISSION ACCOMPLISHED";
        Font font = new Font("Comic Sans", Font.BOLD, 18);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    private void dranNewGame(Graphics g) {

        String message = "PRESS ENTER TO START A NEW GAME";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, 350);
    }
    
    private void dranPressEnter(Graphics g) {

        String message = "PRESS ENTER TO CONTINUE";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, 400);
    }
    
    private void drawGameOver(Graphics g) {

        String message = "Game Over";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    public int getScore() {
		return score;
	}
    
    public int getDeadAliens() {
		return deadAliens;
	}

	public void setDeadAliens(int deadAliens) {
		this.deadAliens = deadAliens;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public int getLevel() {
		return level;
	}

	private void checkCollision(){
    	Rectangle Aliens;
    	Rectangle Missiles;
    	Rectangle Spaceship = spaceship.getBounds();
    	List<Missile> missiles = spaceship.getMissiles();
    	
    	//Colisão Spaceship-Alien
    	for (int i = 0; i < aliens.size(); i++) {
			Alien tAlien = aliens.get(i);
			Aliens = tAlien.getBounds();

			if (Spaceship.intersects(Aliens)) {
				spaceship.setVisible(false);
				tAlien.setVisible(false);
				updateExplosion();
				isAlive = false;
				aliens.get(0);
				Alien.setCount(0);
				level = 0;
				deadAliens = 0;
				Ranking.saveRank(this);
				
				
			}
		}
    	
    	//Colisão Missile-Alien
    	for (int j = 0; j < missiles.size(); j++) {
			Missile tMissile = missiles.get(j);
			Missiles = tMissile.getBounds();
			
	    	for (int i = 0; i < aliens.size(); i++) {
				Alien tAlien = aliens.get(i);
				Aliens = tAlien.getBounds();

				if (Missiles.intersects(Aliens)) {
					tMissile.setVisible(false);
					tAlien.setVisible(false);					
					deadAliens++;
					if(deadAliens <= 50)
					score += 5;
					if(deadAliens > 50 && deadAliens <= 100)
						score += 10;
					if(deadAliens <= 150 && deadAliens > 100)
						score += 15;
					if(deadAliens == 150)
						Ranking.saveRank(this);
					
					
				}
	    	}
    	}
    	
    	
    }
    
   private void updateExplosion(){
	   spaceship.explosion();
   }
    
    
    private void updateSpaceship() {
        spaceship.move();
    }
  

    private class KeyListerner extends KeyAdapter {
        
    @Override
    public void keyPressed(KeyEvent e) {
        
    	
    	if (e.getKeyCode() == KeyEvent.VK_ENTER) {
        	
        	if(aliens.size() == 0 && deadAliens > 0)	
				level++;
        	
        	
        	
        	if(level == 4){
                app.gameOver();
            }
        	
        	if((aliens.size() == 0 && level < 3) || isAlive == false){	
        		spawnAliens();
        	}
        	
        	
            if(deadAliens == 0)
        		score = 0;
        		
                
            if(isAlive == false){
            	
            	
            	isAlive = true;
                score = 0;
                spaceship.noThrust();
                spaceship.setX(SPACESHIP_X);
                spaceship.setY(SPACESHIP_Y);
            }
    	}
        	
        	if(isAlive == true)
        		spaceship.keyPressed(e);
        	
        
        	
     }

     @Override
     public void keyReleased(KeyEvent e) {
            if(isAlive == true)
            	spaceship.keyReleased(e);
        }

        
    }
    
}