package ep2;
import java.awt.Image;
import java.awt.Rectangle;

public class Missile extends Sprite{

	private static final int SPEED = 5;
	private static final int MIN_HEIGHT = 0;
	
	public Missile(int x, int y) {
		super(x,y);

		loadImage("images/missile.png");
	}

	public void move() {
		this.y -= SPEED;
		if (this.y < MIN_HEIGHT) {		//limitação da visibilidade do missil
			visible = false;
		}
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);

	}

	public Image getImage() {
		return image;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public int getHeight(){
        return height;
    }

	@Override
	public int getWidth(){
        return width;
    }

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setVisible(Boolean enable) {
        visible = enable;
    }
    
}