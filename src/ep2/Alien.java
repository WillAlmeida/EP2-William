package ep2;
import java.awt.Image;
import java.awt.Rectangle;

public class Alien extends Sprite {

	private static double SPEED;
	private static final int MAX_HEIGHT = 500;
	public static int count = 0;
	public Alien(int x, int y) {
        super(x, y);
        
        if(count >= 50 && count < 101){
        	alienMedium();
        	SPEED = -1;
        }
        if(count >= 100 && count < 151){		
        	alienHard();
        	SPEED = -1;
		}
        if(count < 50){		
        	alienEasy();
        	SPEED = -1;
        }
        if(count == 149)
        	count = -1;
        
        count++;
        
    }
	


	public void alienEasy(){
		loadImage("images/alien_EASY.png");
	}
	
	public void alienMedium(){
		loadImage("images/alien_MEDIUM.png");
	}
	
	public void alienHard(){
		loadImage("images/alien_HARD.png");
	}
	
	public void move() {
		if(this.y > MAX_HEIGHT){
				this.y = 0;
		}
		else
			this.y -= SPEED;
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);

	}

	public Image getImage() {
		return image;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public int getHeight(){
        return height;
    }

	@Override
	public int getWidth(){
        return width;
    }

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setVisible(Boolean enable) {
        visible = enable;
    }


	public static void setCount(int count) {
		Alien.count = count;
	}
	
	
	public static int getCount() {
		return count;
	}
	
      
    
	
}
